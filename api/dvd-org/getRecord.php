<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

$movieId = null;
$room = null;
$lang = "en";
$sqlForFilter = '';

if (isset($_REQUEST["movieId"]) && $_REQUEST["movieId"] != null && strlen($_REQUEST["movieId"]) > 1) {
    $movieId = $_REQUEST["movieId"];
    $sqlForFilter = $sqlForFilter . " AND moviedetail.movieId = '" . $movieId . "' ";
}

if (isset($_REQUEST["room"]) && $_REQUEST["room"] != null && strlen($_REQUEST["room"]) > 1) {
    $room = $_REQUEST["room"];
    $sqlForFilter .= $sqlForFilter . " AND hist.roomId = '" . $room . "' ";
}

if (isset($_REQUEST["lang"])) {
    $lang = $_REQUEST["lang"];
}

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "SELECT 
                hist.id As id,
                hist.roomId As room,
                inventory.assetId As assetId,
                moviedetail.movieId As movieId,
                moviedetail.movieTitle As title,
                moviedetail.poster As poster,
                moviedetail.year As year,
                hist.statusId As statusId,
                hist.requestTime As requesttime
        
            FROM movie_borrow_history hist 
            
            LEFT JOIN 
                (SELECT movies.id As movieId, 
                (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS movieTitle,
                movies.year As year,
                movies.posterurl As poster
                 FROM movies 
                 INNER JOIN movie_dictionary
                 ON movies.titleId = movie_dictionary.id) moviedetail
            ON moviedetail.movieId = hist.movieId
            
            LEFT JOIN (SELECT movie_inventory.id As inventoryId, movie_inventory.assetId As assetId 
                        FROM movie_inventory) inventory
            ON inventory.inventoryId = hist.inventoryId 
            
            WHERE enable = 1 " . $sqlForFilter . " 
            ORDER BY hist.requestTime DESC;";

$st = $conn->prepare($sql);
$st->execute();

$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {

    $currentMovieId = $row["movieId"];

    $genreString = null;
    $genreId = null;
    $sql = "SELECT DISTINCT title.genreTitle As genreTitle, movies_genre.genreId As genreId 
                  FROM movies_genre
            
                INNER JOIN
                    (SELECT movie_genre.id AS id, 
                    (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS genreTitle
                      FROM movie_genre
                      INNER JOIN movie_dictionary
                      ON movie_genre.titleId = movie_dictionary.id) title
                ON title.id = movies_genre.genreId
                WHERE movies_genre.movieId = '" . $currentMovieId . "' ";

    $st2 = $conn->prepare($sql);
    $st2->execute();



    while ($row2 = $st2->fetch(PDO::FETCH_ASSOC)) {

        if ($genreString == null) {
            $genreString = $row2["genreTitle"];
        } else {
            $genreString = $genreString . ", " . $row2["genreTitle"];
        }

        if ($genreId == null) {
            $genreId = $row2["genreId"];
        } else {
            $genreId = $genreId . "," . $row2["genreId"];
        }

    }
    $row["genre"] = $genreString;
    $row["genreId"] = $genreId;

    $list[] = $row;
}

$conn = null;

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get record good', $list);
} else {
    echo returnStatus(0, 'get record fail');
}
?>
