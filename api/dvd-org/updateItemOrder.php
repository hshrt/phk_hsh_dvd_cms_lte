<?php

ini_set( "display_errors", true );
require("../../config.php");
require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();
include("../checkSession.php");

$type = $_POST["type"];
$orderArr = json_decode($_POST['orderArr']);
$idArr= explode(",",$_POST['idArr']);



if ( empty($idArr) || empty($orderArr)){
    echo returnStatus(0, 'missing input');
    exit;
}else {

    $conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
    $conn->exec("set names utf8");

    if ($type == "genre") {

        $sql = "UPDATE movie_genre SET lastUpdate=now(), lastUpdateBy=:lastUpdateBy, movie_genre.ordering = CASE id ";
        $csv = "";

        for ($x = 0; $x < sizeof($idArr); $x++) {
            $sql .= sprintf("WHEN '%s' THEN %d ", $idArr[$x], $orderArr[$x]);
            $csv .= sprintf("'%s',", $idArr[$x]);
        }
        $csv = substr($csv, 0, -1);
        //$csv = implode(',', $idArr);
        $sql .= "END WHERE id IN ($csv)";

        //echo($sql);
        // exit;
        //$sql = "INSERT INTO mediaItemMap (id,mediaId,itemId,lastupdateTime ) VALUES (UUID() ,:mediaId, :itemId, now())";
        $st = $conn->prepare($sql);

        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);

        $st->execute();

        echo returnStatus(1, 'update movie genre order success');

    } else if ($type == "latest") {

        $sql = "UPDATE movie_latest SET lastUpdate=now(), lastUpdateBy=:lastUpdateBy, movie_latest.ordering = CASE id ";
        $csv = "";

        for ($x = 0; $x < sizeof($idArr); $x++) {
            $sql .= sprintf("WHEN '%s' THEN %d ", $idArr[$x], $orderArr[$x]);
            $csv .= sprintf("'%s',", $idArr[$x]);
        }
        $csv = substr($csv, 0, -1);
        //$csv = implode(',', $idArr);
        $sql .= "END WHERE id IN ($csv)";

        //echo($sql);
        // exit;
        //$sql = "INSERT INTO mediaItemMap (id,mediaId,itemId,lastupdateTime ) VALUES (UUID() ,:mediaId, :itemId, now())";
        $st = $conn->prepare($sql);

        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);

        $st->execute();

        echo returnStatus(1, 'update latest movie order success');

    }
}


?>
