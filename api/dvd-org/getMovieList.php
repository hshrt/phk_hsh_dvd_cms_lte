<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$genreId = null;
$lang = null;

if (isset($_REQUEST['genreId'])) {
    $genreId = $_REQUEST['genreId'];
}

if (isset($_REQUEST['lang'])) {
    $lang = $_REQUEST['lang'];
} else {
    $lang = 'en';
}

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "";

if($genreId == null){

    $sql = "SELECT DISTINCT movies.id As movieId, title.movieTitle As movieTitle, movies.posterurl As poster
        FROM movies
        
        INNER JOIN 
            (SELECT DISTINCT movies.titleId AS titleId, 
            (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS movieTitle
             FROM movies 
             INNER JOIN movie_dictionary
             ON movies.titleId = movie_dictionary.id) title
        ON title.titleId = movies.titleId 
        WHERE movies.isVoid = 0 ";

        $sql = $sql."GROUP BY movies.id ORDER BY title.movieTitle ASC;";

} else {

    $sql = "SELECT DISTINCT movies.id As movieId, title.movieTitle As movieTitle, movies.posterurl As poster
        FROM movies
        
        INNER JOIN 
            (SELECT DISTINCT movies.titleId AS titleId, 
            (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS movieTitle
             FROM movies 
             INNER JOIN movie_dictionary
             ON movies.titleId = movie_dictionary.id) title
        ON title.titleId = movies.titleId 
        
        INNER JOIN movies_genre 
        ON movies.id = movies_genre.movieId
        
        WHERE movies_genre.genreId = '".$genreId."' AND movies.isVoid = 0 ";
        $sql = $sql."GROUP BY movies.id ORDER BY title.movieTitle ASC;";
}

$st = $conn->prepare($sql);

$st->execute();

$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}

$conn = null;

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
    echo returnStatus(1, 'get movieList by genre good', $list);
} else {
    echo returnStatus(0, 'get movieList by genre fail', $sql);
}

?>
