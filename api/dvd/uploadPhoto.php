<?php

ini_set( "display_errors", true );
require( "../../config.php" );

require("../../php/inc.appvars.php");

require_once "../../php/func_nx.php";
require_once "../../php/func_json.php";

require("../../php/lib/resize.class.php");

session_start();


$photo_data = $_POST['image'];
$fileExtension = isset($_POST['fileExtension'])?$_POST['fileExtension']:"jpg";

if($fileExtension=="jpeg"){
    $fileExtension = "jpg";
}

$type = isset($_POST['type'])?$_POST['type']:null;
$album = isset($_POST['album'])?$_POST['album']:"";

if ( empty($photo_data)){
    echo returnStatus(0, 'missing_img_data');
    exit;
}else{

    $timestamp = date('m_d_Y_h_i_s', time());
    $file_name = $timestamp . '.' .$fileExtension;
    $file_path = 'upload/'.$file_name;
    $local_file_path = '../../upload/'.$file_name;

    if ( file_put_contents($local_file_path, base64_decode($photo_data)))
    {
        echo returnStatus(1 , 'good', array('filepath' => $file_path));
    } else {
        echo returnStatus(0 , 'error');
    }

}

?>
