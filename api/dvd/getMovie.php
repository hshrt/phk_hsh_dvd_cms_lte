<?php
require("../../config.php");

ini_set("display_errors", true);

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

//using itemId is for get one specific item
//using parentId is for get a list of items which is child of that parent

$page = 0;
$itemPerPage = 15;
$getCount = null;
$movieId = null;
$genre = null;
$lang = "en";

if (isset($_REQUEST['getCount'])) {
    $getCount = $_REQUEST['getCount'];
}

if (isset($_REQUEST['movieId'])) {
    $movieId = $_REQUEST['movieId'];
}

if (isset($_REQUEST['lang'])) {
    $lang = $_REQUEST['lang'];
}

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");


if (!empty($getCount)) {
    $sql = "SELECT count(*) AS totalNum FROM movies ";
    $sql = $sql."WHERE isVoid = 0 ";

    if (isset($_REQUEST['movieId'])) {
        $sql = $sql." AND movies.id = ".$movieId."' ";
    }
    $st = $conn->prepare($sql);
    $st->execute();
    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
        $list[] = $row;
    }

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'get movie count good', $list);
    } else {
        echo returnStatus(0, 'get movie count fail');
    }

} else {

    $sql = "SELECT DISTINCT movies.id As movieId, movies.titleId AS titleId, movies.descriptionId As descriptionId, title.movieTitle AS movieTitle, 
                            description.movieDescription AS movieDescription, movies.year AS year, 
                            movies.divisionId As divisionId, movies.rating As rating, 
                            (CASE WHEN m1.available IS NULL THEN 0 ELSE m1.available END) AS available, 
                            movies.posterurl As poster, (CASE WHEN m2.stock IS NULL THEN 0 ELSE m2.stock END) As stock
            FROM movies
            LEFT JOIN (SELECT COUNT(movie_inventory.id) AS available,
                        movie_inventory.movieId As movieId
                      FROM movie_inventory 
                      WHERE available = 1 AND isVoid = 0 
                      GROUP BY  movie_inventory.movieId) m1
            ON movies.id = m1.movieId
            
             LEFT JOIN (SELECT COUNT(movie_inventory.id) AS stock,
                        movie_inventory.movieId As movieId
                      FROM movie_inventory 
                      WHERE isVoid = 0 
                      GROUP BY  movie_inventory.movieId) m2
            ON movies.id = m2.movieId
            
            INNER JOIN 
                (SELECT movies.titleId AS titleId,
                 (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS movieTitle
                 FROM movies 
                 INNER JOIN movie_dictionary
                 ON movies.titleId = movie_dictionary.id) title
            ON title.titleId = movies.titleId 
            
            INNER JOIN 
                (SELECT movies.descriptionId AS descriptionId,
                (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS movieDescription
                 FROM movies 
                 INNER JOIN movie_dictionary
                 ON movies.descriptionId = movie_dictionary.id) description
            ON description.descriptionId = movies.descriptionId ";

    $sql = $sql."WHERE isVoid = 0 ";

    if (isset($_REQUEST['movieId'])) {
        $sql = $sql." AND movies.id = '".$movieId."' ";
    }
    $sql = $sql."GROUP BY title.movieTitle ORDER BY title.movieTitle ASC;";


    $st = $conn->prepare($sql);
    $st->execute();
    $list = array();

    while ($row = $st->fetch(PDO::FETCH_ASSOC)) {

        $currentMovieId = $row["movieId"];

        $genreString = null;
        $genreId = null;
        $sql = "SELECT DISTINCT title.genreTitle As genreTitle, movies_genre.genreId As genreId 
                  FROM movies_genre
            
                INNER JOIN
                    (SELECT movie_genre.id AS id, 
                    (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS genreTitle
                      FROM movie_genre
                      INNER JOIN movie_dictionary
                      ON movie_genre.titleId = movie_dictionary.id) title
                ON title.id = movies_genre.genreId
                WHERE movies_genre.movieId = '".$currentMovieId."' ";

        $st2 = $conn->prepare($sql);
        $st2->execute();

        while ($row2 = $st2->fetch(PDO::FETCH_ASSOC)) {

            if($genreString == null){
                $genreString = $row2["genreTitle"];
            } else {
                $genreString = $genreString.", ".$row2["genreTitle"];
            }

            if($genreId == null){
                $genreId = $row2["genreId"];
            } else {
                $genreId = $genreId.",".$row2["genreId"];
            }

        }
        $row["genre"] = $genreString;
        $row["genreId"] = $genreId;


        $languageString = null;
        $languageId = null;
        $sql = "SELECT DISTINCT title.languageTitle As languageTitle, movies_language.languageId As languageId 
                  FROM movies_language
            
                INNER JOIN
                    (SELECT movie_language.id AS id, 
                    (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS languageTitle
                      FROM movie_language
                      INNER JOIN movie_dictionary
                      ON movie_language.titleId = movie_dictionary.id) title
                ON title.id = movies_language.languageId
                WHERE movies_language.movieId = '".$currentMovieId."' ";

        $st2 = $conn->prepare($sql);
        $st2->execute();

        while ($row2 = $st2->fetch(PDO::FETCH_ASSOC)) {

            if($languageString == null){
                $languageString = $row2["languageTitle"];
            } else {
                $languageString = $languageString.", ".$row2["languageTitle"];
            }

            if($languageId == null){
                $languageId = $row2["languageId"];
            } else {
                $languageId = $languageId.",".$row2["languageId"];
            }

        }
        $row["language"] = $languageString;
        $row["languageId"] = $languageId;


        $subtitleString = null;
        $subtitleId = null;
        $sql = "SELECT DISTINCT title.subtitleTitle As subtitleTitle, movies_subtitle.subtitleId As subtitleId 
                  FROM movies_subtitle
            
                INNER JOIN
                    (SELECT movie_subtitle.id AS id, 
                    (CASE movie_dictionary." . $lang . " WHEN '' THEN movie_dictionary.en ELSE movie_dictionary." . $lang . " END ) AS subtitleTitle
                      FROM movie_subtitle
                      INNER JOIN movie_dictionary
                      ON movie_subtitle.titleId = movie_dictionary.id) title
                ON title.id = movies_subtitle.subtitleId
                WHERE movies_subtitle.movieId = '".$currentMovieId."' ";

        $st2 = $conn->prepare($sql);
        $st2->execute();

        while ($row2 = $st2->fetch(PDO::FETCH_ASSOC)) {

            if($subtitleString == null){
                $subtitleString = $row2["subtitleTitle"];
            } else {
                $subtitleString = $subtitleString.", ".$row2["subtitleTitle"];
            }

            if($subtitleId == null){
                $subtitleId = $row2["subtitleId"];
            } else {
                $subtitleId = $subtitleId.",".$row2["subtitleId"];
            }

        }
        $row["subtitle"] = $subtitleString;
        $row["subtitleId"] = $subtitleId;


        $list[] = $row;
    }

    $conn = null;

    if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {
        echo returnStatus(1, 'get movie good', $list);
    } else {
        echo returnStatus(0, 'get movie fail');
    }
}

?>
