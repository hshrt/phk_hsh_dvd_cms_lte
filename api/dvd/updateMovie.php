<?php

ini_set("display_errors", true);

require("../../config.php");
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");

session_start();

$movieId = isset($_POST['movieId']) ? $_POST['movieId'] : null;
$genreId = isset($_POST['genreId']) ? $_POST['genreId'] : null;
$year = isset($_POST['year']) ? $_POST['year'] : null;
$posterurl = isset($_POST['posterurl']) ? $_POST['posterurl'] : null;
$language = isset($_POST['language']) ? $_POST['language'] : null;
$subtitle = isset($_POST['subtitle']) ? $_POST['subtitle'] : null;
$rating = isset($_POST['rating']) ? $_POST['rating'] : null;
$divisionId = isset($_POST['divisionId']) ? $_POST['divisionId'] : null;

//setup DB
$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

$sql = "UPDATE movies SET year=:year,posterurl=:posterurl,rating=:rating, divisionId=:divisionId, lastUpdate=now(),lastUpdateBy=:lastUpdateBy WHERE id = :movieId;";

//echo $sql;

$st = $conn->prepare($sql);

$st->bindValue(":movieId", $movieId, PDO::PARAM_STR);
$st->bindValue(":year", $year, PDO::PARAM_STR);
$st->bindValue(":posterurl", $posterurl, PDO::PARAM_STR);
$st->bindValue(":rating", $rating, PDO::PARAM_INT);
$st->bindValue(":divisionId", $divisionId, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);

$st->execute();

if ($st->fetchColumn() > 0 || $st->rowCount() > 0) {


    $sql = "DELETE FROM movies_genre WHERE movieId = '" .$movieId."'" ;
    $st = $conn->prepare($sql);
    $st->execute();

    $genreArray = explode(',', $genreId);
    foreach ($genreArray as &$value){
        $sql = "INSERT INTO 
            movies_genre (movieId, genreId, lastUpdate, lastUpdateBy) 
            VALUES (:movieId, :genreId,  now(),:lastUpdateBy)";
        $st = $conn->prepare($sql);
        $st->bindValue(":movieId", $movieId, PDO::PARAM_STR);
        $st->bindValue(":genreId", $value, PDO::PARAM_STR);
        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
        $st->execute();
    }

    $sql = "DELETE FROM movies_language WHERE movieId = '" .$movieId."'" ;
    $st = $conn->prepare($sql);
    $st->execute();

    $languageArray = explode(',', $language);
    foreach ($languageArray as &$value){
        $sql = "INSERT INTO 
            movies_language (movieId, languageId, lastUpdate, lastUpdateBy) 
            VALUES (:movieId, :languageId,  now(), :lastUpdateBy)";
        $st = $conn->prepare($sql);
        $st->bindValue(":movieId", $movieId, PDO::PARAM_STR);
        $st->bindValue(":languageId", $value, PDO::PARAM_STR);
        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
        $st->execute();
    }

    $sql = "DELETE FROM movies_subtitle WHERE movieId = '" .$movieId."'" ;
    $st = $conn->prepare($sql);
    $st->execute();

    $subtitleArray = explode(',', $subtitle);
    foreach ($subtitleArray as &$value){
        $sql = "INSERT INTO 
            movies_subtitle (movieId, subtitleId, lastUpdate, lastUpdateBy) 
            VALUES (:movieId, :subtitleId,  now(), :lastUpdateBy)";
        $st = $conn->prepare($sql);
        $st->bindValue(":movieId", $movieId, PDO::PARAM_STR);
        $st->bindValue(":subtitleId", $value, PDO::PARAM_STR);
        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
        $st->execute();
    }
    echo returnStatus(1, 'update movie good');
} else {
    echo returnStatus(0, 'update movie fail');
}


$conn = null;


?>
