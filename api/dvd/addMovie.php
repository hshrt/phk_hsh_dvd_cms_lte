<?php

ini_set("display_errors", true);
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$title_zh_cn = isset($_POST['title_zh_cn']) ? $_POST['title_zh_cn'] : '';
$title_en = isset($_POST['title_en']) ? $_POST['title_en'] : '';
$title_zh_hk = isset($_POST['title_zh_hk']) ? $_POST['title_zh_hk'] : '';
$title_jp = isset($_POST['title_jp']) ? $_POST['title_jp'] : '';
$title_fr = isset($_POST['title_fr']) ? $_POST['title_fr'] : '';
$title_ar = isset($_POST['title_ar']) ? $_POST['title_ar'] : '';
$title_es = isset($_POST['title_es']) ? $_POST['title_es'] : '';
$title_de = isset($_POST['title_de']) ? $_POST['title_de'] : '';
$title_ko = isset($_POST['title_ko']) ? $_POST['title_ko'] : '';
$title_ru = isset($_POST['title_ru']) ? $_POST['title_ru'] : '';
$title_pt = isset($_POST['title_pt']) ? $_POST['title_pt'] : '';

$description_zh_cn = isset($_POST['description_zh_cn']) ? $_POST['description_zh_cn'] : '';
$description_en = isset($_POST['description_en']) ? $_POST['description_en'] : '';
$description_zh_hk = isset($_POST['description_zh_hk']) ? $_POST['description_zh_hk'] : '';
$description_jp = isset($_POST['description_jp']) ? $_POST['description_jp'] : '';
$description_fr = isset($_POST['description_fr']) ? $_POST['description_fr'] : '';
$description_ar = isset($_POST['description_ar']) ? $_POST['description_ar'] : '';
$description_es = isset($_POST['description_es']) ? $_POST['description_es'] : '';
$description_de = isset($_POST['description_de']) ? $_POST['description_de'] : '';
$description_ko = isset($_POST['description_ko']) ? $_POST['description_ko'] : '';
$description_ru = isset($_POST['description_ru']) ? $_POST['description_ru'] : '';
$description_pt = isset($_POST['description_pt']) ? $_POST['description_pt'] : '';

$genreId = isset($_POST['genreId']) ? $_POST['genreId'] : '';
$language = isset($_POST['language']) ? $_POST['language'] : '';
$subtitle = isset($_POST['subtitle']) ? $_POST['subtitle'] : '';
$year = isset($_POST['year']) ? $_POST['year'] : '';
$poster = isset($_POST['poster']) ? $_POST['poster'] : '';
$rating = isset($_POST['rating']) ? $_POST['rating'] : null;
$divisionId = isset($_POST['divisionId']) ? $_POST['divisionId'] : null;

$conn = new PDO(DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

//*****create Dictionary for Subject
$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare($sql);
$st->execute();

$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}
$uuid = $list[0]["UUID"];

$sql = "INSERT INTO movie_dictionary (id,en,zh_hk,zh_cn,jp,fr,ar,es,de,ko,ru,pt,type,lastUpdate, lastUpdateBy) VALUES (:id,
:en, :zh_hk, :zh_cn, :jp, :fr, :ar, :es, :de, :ko, :ru, :pt,:type, now(), :lastUpdateBy)";
$st = $conn->prepare($sql);
$st->bindValue(":id", $uuid, PDO::PARAM_STR);
$st->bindValue(":en", $title_en, PDO::PARAM_STR);
$st->bindValue(":zh_hk", $title_zh_hk, PDO::PARAM_STR);
$st->bindValue(":zh_cn", $title_zh_cn, PDO::PARAM_STR);
$st->bindValue(":jp", $title_jp, PDO::PARAM_STR);
$st->bindValue(":fr", $title_fr, PDO::PARAM_STR);
$st->bindValue(":ar", $title_ar, PDO::PARAM_STR);
$st->bindValue(":es", $title_es, PDO::PARAM_STR);
$st->bindValue(":de", $title_de, PDO::PARAM_STR);
$st->bindValue(":ko", $title_ko, PDO::PARAM_STR);
$st->bindValue(":ru", $title_ru, PDO::PARAM_STR);
$st->bindValue(":pt", $title_pt, PDO::PARAM_STR);
$st->bindValue(":type", "movie", PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
$st->execute();
$titleId = $uuid;

$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare($sql);
$st->execute();

$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}
$uuid = $list[0]["UUID"];

$sql = "INSERT INTO movie_dictionary (id,en,zh_hk,zh_cn,jp,fr,ar,es,de,ko,ru,pt,type,lastUpdate, lastUpdateBy) VALUES (:id,
:en, :zh_hk, :zh_cn, :jp, :fr, :ar, :es, :de, :ko, :ru, :pt,:type, now(), :lastUpdateBy)";
$st = $conn->prepare($sql);
$st->bindValue(":id", $uuid, PDO::PARAM_STR);
$st->bindValue(":en", $description_en, PDO::PARAM_STR);
$st->bindValue(":zh_hk", $description_zh_hk, PDO::PARAM_STR);
$st->bindValue(":zh_cn", $description_zh_cn, PDO::PARAM_STR);
$st->bindValue(":jp", $description_jp, PDO::PARAM_STR);
$st->bindValue(":fr", $description_fr, PDO::PARAM_STR);
$st->bindValue(":ar", $description_ar, PDO::PARAM_STR);
$st->bindValue(":es", $description_es, PDO::PARAM_STR);
$st->bindValue(":de", $description_de, PDO::PARAM_STR);
$st->bindValue(":ko", $description_ko, PDO::PARAM_STR);
$st->bindValue(":ru", $description_ru, PDO::PARAM_STR);
$st->bindValue(":pt", $description_pt, PDO::PARAM_STR);
$st->bindValue(":type", "movie", PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
$st->execute();
$descriptionId = $uuid;

$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare($sql);
$st->execute();

$list = array();

while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
    $list[] = $row;
}
$uuid = $list[0]["UUID"];

$sql = "INSERT INTO 
            movies (id, titleId, descriptionId, year, posterurl, lastUpdate, lastUpdateBy) 
            VALUES (:id, :titleId, :descriptionId, :year, :posterurl, now(),:lastUpdateBy)";
$st = $conn->prepare($sql);
$st->bindValue(":id", $uuid, PDO::PARAM_STR);
$st->bindValue(":titleId", $titleId, PDO::PARAM_STR);
$st->bindValue(":descriptionId", $descriptionId, PDO::PARAM_STR);
$st->bindValue(":year", $year, PDO::PARAM_STR);
$st->bindValue(":posterurl", $poster, PDO::PARAM_STR);
$st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
$st->execute();

if ($st->rowCount() > 0) {

    $genreArray = explode(',', $genreId);
    foreach ($genreArray as &$value){
        $sql = "INSERT INTO 
            movies_genre (movieId, genreId, lastUpdate, lastUpdateBy) 
            VALUES (:movieId, :genreId,  now(),:lastUpdateBy)";
        $st = $conn->prepare($sql);
        $st->bindValue(":movieId", $uuid, PDO::PARAM_STR);
        $st->bindValue(":genreId", $value, PDO::PARAM_STR);
        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
        $st->execute();
    }

    $languageArray = explode(',', $language);
    foreach ($languageArray as &$value){
        $sql = "INSERT INTO 
            movies_language (movieId, languageId, lastUpdate, lastUpdateBy) 
            VALUES (:movieId, :languageId,  now(), :lastUpdateBy)";
        $st = $conn->prepare($sql);
        $st->bindValue(":movieId", $uuid, PDO::PARAM_STR);
        $st->bindValue(":languageId", $value, PDO::PARAM_STR);
        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
        $st->execute();
    }

    $subtitleArray = explode(',', $subtitle);
    foreach ($subtitleArray as &$value){
        $sql = "INSERT INTO 
            movies_subtitle (movieId, subtitleId, lastUpdate, lastUpdateBy) 
            VALUES (:movieId, :subtitleId,  now(), :lastUpdateBy)";
        $st = $conn->prepare($sql);
        $st->bindValue(":movieId", $uuid, PDO::PARAM_STR);
        $st->bindValue(":subtitleId", $value, PDO::PARAM_STR);
        $st->bindValue(":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR);
        $st->execute();
    }

    echo returnStatus(1, 'Add movie OK ');
} else {
    echo returnStatus(0, 'Add moive fail');
}


return 0;

?>
