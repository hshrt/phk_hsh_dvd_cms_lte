<!-- popup box setup -->
<div id="subTopBarContainer">
    <div id="ulContainer">
        <ul>
            <li class="" id="stagingli">
                <button class="btn btn-danger btn-xs" id="stagingBtn">Staging Server</button>
            </li>
            <li class=""><span class="	glyphicon glyphicon-question-sign" id="requestLogo"></span>
                <a id="requestTab">Request Lists</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-film" id="inventoryLogo"></span>
                <a id="inventoryTab">Movie & Inventory</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-star-empty" id="latestLogo"></span>
                <a id="latestTab">Latest</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-flash" id="genreLogo"></span>
                <a id="genreTab">Genre</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-bullhorn" id="languageLogo"></span>
                <a id="languageTab">Language</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-font" id="subtitleLogo"></span>
                <a id="subtitleTab">Subtitle</a>
            </li>
            <li class=""><span class="glyphicon glyphicon-cog" id="configLogo"></span>
                <a id="configTab">Config</a>
            </li>
        </ul>
    </div>
</div>
